###
  core.coffee
###

#================
#params
#================
system = window.system = {}
#================

#================
#function
#================
#selector
$$ = window.$$ = (selector) ->
  h = system.handle or= {}
  if s = selector
    h[s] or h[s] = $ s
  else h

#log
$.log = (param) -> console?.log? param
#================