$ ->

  width = $(window).width()
  height = $(window).height()

  if !window.Worker
    $ '#section-a, #section-b'
    .css
      width: width
      height: height

  $ '#stage'
  .fullpage
    navigation: true