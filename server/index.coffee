#app
app = (require 'express')()

#config
app.settings['x-powered-by'] = false #disabled 'x-powered-by: express'
app.listen 8080 #port

#router
app.get '*', (req, res) ->
  #header
  res.header 'X-Powered-By', 'Mimiko'
  #send
  res.sendfile './build/' + (req.url or 'index.html')