#============
#require
#============
#exec
exec = (require 'child_process').exec

#gulp
gulp = require 'gulp'

#tool
gutil = require 'gulp-util'
watch = require 'gulp-watch'
plumber = require 'gulp-plumber'
clean = require 'gulp-clean'
ignore = require 'gulp-ignore'
rename = require 'gulp-rename'
next = require 'gulp-callback'
#gulpif = require 'gulp-if'
#concat = require 'gulp-concat'
#replace = require 'gulp-replace'
#include = require 'gulp-include'

#type
jade = require 'gulp-jade'
coffee = require 'gulp-coffee'
stylus = require 'gulp-stylus'
cson = require 'gulp-cson'

#advanced tool

#minify
uglify = require 'gulp-uglify'
minCss = require 'gulp-minify-css'

#lint
lint = require 'gulp-coffeelint'

#livereload
reload = require 'gulp-livereload'

#============
#error
#============
#uncaughtException
process.on 'uncaughtException', (err) -> $.log err.stack

#============
#function $
#============
$ = {}
#log
$.log = console.log
#info
$.info = (msg) ->
  d = new Date()
  [h, m, s] = [d.getHours(), d.getMinutes(), d.getSeconds()]
  if m < 10 then m = '0' + m
  if s < 10 then s = '0' + s
  t = h + ':' + m + ':' + s
  $.log '[' + t + '] ' + msg
  #return
  msg

#now
$.now = Date.now

#============
#param
#============
#base
base = process.cwd()

#path
path =
  gulp: 'gulpfile.coffee'
  source: './source/'
  build: './build/'
  server: './server/'
path.min = '.min'
path.jade = path.source + '**/*.jade'
path.stylus = path.source + '**/*.styl'
path.coffee = path.source + '**/*.coffee'
path.cson = path.source + '**/*.cson'
path.image = path.source + '**/*.{png,jpg,gif}'

#============
#watch
#============
gulp.task 'watch', ->

  #livereload
  reload.listen()

  #image
  watch path.image, base: path.source
  .pipe gulp.dest path.build

  #stylus
  watch path.stylus, base: path.source
  .pipe plumber()
  .pipe ignore '*.ex.styl'
  .pipe stylus compress: true
  .pipe rename suffix: path.min
  .pipe gulp.dest path.build
  #reload
  .pipe reload()

  #css
  #*.css
  watch path.source + '**/*.css', base: path.source
  .pipe ignore '*.min.css'
  .pipe minCss()
  .pipe rename suffix: path.min
  .pipe gulp.dest path.build
  #*.min.css
  watch path.source + '**/*.min.css', base: path.source
  .pipe gulp.dest path.build

  #coffee
  watch path.coffee, base: path.source
  .pipe plumber()
  #lint
  .pipe lint()
  .pipe lint.reporter()
  #include
  #.pipe include()
  #coffee
  .pipe coffee()
  #uglify
  .pipe uglify()
  .pipe rename suffix: path.min
  .pipe gulp.dest path.build

  #js
  #*.js
  watch path.source + '**/*.js', base: path.source
  .pipe ignore '*.min.js'
  .pipe uglify()
  .pipe rename suffix: path.min
  .pipe gulp.dest path.build
  #*.min.js
  watch path.source + '**/*.min.js', base: path.source
  .pipe gulp.dest path.build

  #jade
  watch path.jade, base: path.source
  .pipe plumber()
  .pipe ignore '**/framework/**/*.jade'
  .pipe jade()
  .pipe gulp.dest path.build

  #core.stylus
  watch path.source + 'framework/style/import/**/*.styl', ->
    gulp.src path.source + 'framework/style/core.styl'
    .pipe stylus compress: true
    .pipe rename suffix: path.min
    .pipe gulp.dest path.build + 'framework/style/'
    #reload
    .pipe reload()

#============
#build
#============
gulp.task 'build', ->

  #prepare build
  gulp.src path.gulp
  .pipe gulp.dest path.build
  .pipe next ->

    #clean
    gulp.src path.build
    .pipe clean()
    .pipe next ->

      #icon
      gulp.src path.source + 'icon/font/*.*'
      .pipe gulp.dest path.build + 'icon/font/'

      #image
      gulp.src path.image, base: path.source
      .pipe gulp.dest path.build

      #stylus
      gulp.src path.stylus, base: path.source
      .pipe plumber()
      .pipe ignore '*.ex.styl'
      .pipe stylus compress: true
      .pipe rename suffix: path.min
      .pipe gulp.dest path.build

      #css
      #*.css
      gulp.src path.source + '**/*.css', base: path.source
      .pipe ignore '*.min.css'
      .pipe minCss()
      .pipe rename suffix: path.min
      .pipe gulp.dest path.build
      #*.min.css
      gulp.src path.source + '**/*.min.css', base: path.source
      .pipe gulp.dest path.build

      #coffee
      gulp.src path.coffee, base: path.source
      .pipe plumber()
      #lint
      .pipe lint()
      .pipe lint.reporter()
      #include
      #.pipe include()
      #coffee
      .pipe coffee()
      #uglify
      .pipe uglify()
      .pipe rename suffix: path.min
      .pipe gulp.dest path.build

      #js
      #*.js
      gulp.src path.source + '**/*.js', base: path.source
      .pipe ignore '*.min.js'
      .pipe uglify()
      .pipe rename suffix: path.min
      .pipe gulp.dest path.build
      #*.min.js
      gulp.src path.source + '**/*.min.js', base: path.source
      .pipe gulp.dest path.build

      #jade
      gulp.src path.jade, base: path.source
      .pipe plumber()
      .pipe ignore '**/framework/**/*.jade'
      .pipe jade()
      .pipe gulp.dest path.build